FROM docker.elastic.co/elasticsearch/elasticsearch:8.7.0-amd64

RUN bin/elasticsearch-plugin install analysis-icu
